package client

import (
	"net/http"
	"time"
)

const (
	endpoint = "https://home.solarmanpv.com"
)

type Client struct {
	Username    string
	Password    string
	AccessToken string

	HTTPClient *http.Client
}

// New returns a configured Client.
func New(username, password string) (*Client, error) {
	c := &Client{
		Username: username,
		Password: password,

		HTTPClient: &http.Client{
			Timeout: time.Second * 30,
		},
	}

	token, err := c.Login()
	if err != nil {
		return nil, err
	}

	c.AccessToken = token

	return c, nil
}
