package client

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type AuthResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"`
	Scope        string `json:"scope"`
	JTI          string `json:"jti"`
}

// Login issues the login request.
// Required to perform authenticated requests afterwards.
func (c *Client) Login() (string, error) {
	req, err := createLoginRequest(c.Username, c.Password)
	if err != nil {
		return "", err
	}

	response, err := c.HTTPClient.Do(req)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	if response.StatusCode == http.StatusOK {
		bodyBytes, err := io.ReadAll(response.Body)
		if err != nil {
			return "", err
		}

		res := AuthResponse{}
		err = json.Unmarshal(bodyBytes, &res)
		if err != nil {
			return "", err
		}
		return res.AccessToken, nil
	}

	return "", fmt.Errorf("bad status code %d", response.StatusCode)
}

func createLoginRequest(username, password string) (*http.Request, error) {
	h := sha256.New()
	h.Write([]byte(password))
	passwordHash := fmt.Sprintf("%x", h.Sum(nil))

	payload := url.Values{}
	payload.Set("grant_type", "password")
	payload.Set("username", username)
	payload.Set("clear_text_pwd", password)
	payload.Set("password", passwordHash)
	payload.Set("identity_type", "2")
	payload.Set("client_id", "test")

	encodedData := payload.Encode()
	urlPath := endpoint + "/oauth-s/oauth/token"
	req, err := http.NewRequest(http.MethodPost, urlPath, strings.NewReader(encodedData))
	if err != nil {
		return req, err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(encodedData)))

	return req, nil
}
