package client

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type MonthResponse struct {
	Records []DayRecord `json:"records"`
}

type DayRecord struct {
	Id                string  `json:"id"`
	PlantId           int     `json:"systemId"`
	Year              int     `json:"year"`
	Month             int     `json:"month"`
	Day               int     `json:"day"`
	GenerationValue   float64 `json:"generationValue,omitempty"`
	FullPowerHoursDay float64 `json:"fullPowerHoursDay,omitempty"`
}

func (c *Client) GetMonth(plantId, year, month int) ([]DayRecord, error) {
	urlPath := fmt.Sprintf(
		"%s/maintain-s/history/power/%d/stats/month?year=%d&month=%d",
		endpoint,
		plantId,
		year,
		month)

	req, err := http.NewRequest(http.MethodGet, urlPath, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+c.AccessToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json;charset=UTF-8")

	response, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	bodyBytes, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("bad status code %d", response.StatusCode)
	}

	res := MonthResponse{}
	err = json.Unmarshal(bodyBytes, &res)
	if err != nil {
		return nil, err
	}

	return res.Records, nil
}
