package client

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

type PlantsResponse struct {
	Total int     `json:"total"`
	Data  []Plant `json:"data"`
}

type Plant struct {
	Id int `json:"id"`
}

// GetPlants returns an array of plants (solar power plants)
// owned by the user.
func (c *Client) GetPlants() ([]Plant, error) {
	urlPath := endpoint + "/maintain-s/operating/station/search?order.direction=DESC&order.property=id&page=1&size=20"
	req, err := http.NewRequest(http.MethodPost, urlPath, strings.NewReader("{}"))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+c.AccessToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json;charset=UTF-8")

	response, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	bodyBytes, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if response.StatusCode == http.StatusOK {
		res := PlantsResponse{}
		err = json.Unmarshal(bodyBytes, &res)
		if err != nil {
			return nil, err
		}
		return res.Data, nil
	}

	return nil, fmt.Errorf("bad status code %d", response.StatusCode)
}
