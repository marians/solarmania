package client

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

type DayResponse struct {
	Records []FiveMinuteRecord `json:"records"`
}

type FiveMinuteRecord struct {
	Id                 string  `json:"id"`
	PlantId            int     `json:"systemId"`
	DateTime           float64 `json:"dateTime"`
	GenerationPower    float64 `json:"generationPower,omitempty"`
	GenerationCapacity float64 `json:"generationCapacity,omitempty"`
}

// GetDay returns data for a single plant for one
// day, in 5 minute intervals.
func (c *Client) GetDay(plantId, year, month, day int) ([]FiveMinuteRecord, error) {
	urlPath := fmt.Sprintf("%s/maintain-s/history/power/%d/record?year=%d&month=%d&day=%d",
		endpoint, plantId, year, month, day)
	req, err := http.NewRequest(http.MethodGet, urlPath, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Authorization", "Bearer "+c.AccessToken)
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json;charset=UTF-8")

	response, err := c.HTTPClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	bodyBytes, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	if response.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("bad status code %d", response.StatusCode)
	}

	res := DayResponse{}
	err = json.Unmarshal(bodyBytes, &res)
	if err != nil {
		return nil, err
	}

	return res.Records, nil
}
