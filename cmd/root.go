package cmd

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/spf13/cobra"

	"codeberg.org/marians/solarmania/pkg/client"
)

var (
	rootCmd = &cobra.Command{
		Use:   "solarmania",
		Short: "Fetch production data from a solarman plant",
		Long: `Export production data from Solarman.

Two different export types are available:

- Daily production data in kilowatt hours. Use --resolution 1d
  for this type and optionally specify the month to export using
  --year and --month.

- Fine grained (5 minute intervals) power data in watt. Use
  --resolution 5m for this type. Specify the day to export for
  using --year, --month, and --day.

Currently we assume that you have one plant configured. If you have
multiple plants, only data for the first one will be exported.
`,
		Example: `
  # Daily production in kilowatt hours for the month of September 2022
  solarmania \
   --username mail@example.com \
   --password SECRET \
   --resolution 1d \
   --year 2022 --month 9

  # Fine-grained power data in Watt for September 12, 2022
  solarmania \
   --username mail@example.com \
   --password SECRET \
   --resolution 5m \
   --year 2022 --month 9 --day 12
`,
		Run: runRoot,
	}

	username   string
	password   string
	resolution string
	year       int
	month      int
	day        int
)

func Execute() error {
	return rootCmd.Execute()
}

func init() {
	defaultYear, defaultMonth, defaultDay := time.Now().UTC().Date()

	rootCmd.PersistentFlags().StringVar(&username, "username", "", "user name or email to authenticate with")
	rootCmd.PersistentFlags().StringVar(&password, "password", "", "password to authenticate with")
	rootCmd.PersistentFlags().IntVar(&year, "year", defaultYear, "Year to export")
	rootCmd.PersistentFlags().IntVar(&month, "month", int(defaultMonth), "Month to export")
	rootCmd.PersistentFlags().IntVar(&day, "day", defaultDay, "Day to export")
	rootCmd.PersistentFlags().StringVar(&resolution, "resolution", "1d", "resolution, either \"5m\" for five minutes or \"1d\" for daily")

	rootCmd.MarkFlagRequired("username")
	rootCmd.MarkFlagRequired("password")
}

func runRoot(cmd *cobra.Command, args []string) {
	c, err := client.New(username, password)
	if err != nil {
		log.Fatal(err)
	}

	plants, err := c.GetPlants()
	if err != nil {
		log.Fatal(err)
	}

	if len(plants) > 1 {
		os.Stderr.WriteString("Warning: Found more than one plant. Only data for the first one will be exported.\n")
	}

	if resolution == "1d" {
		records, err := c.GetMonth(plants[0].Id, year, month)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("plant_id,date,production_kwh")
		for _, item := range records {
			fmt.Printf("%d,%d-%02d-%02d,%.1f\n", item.PlantId, item.Year, item.Month, item.Day, item.GenerationValue)
		}
	} else {
		records, err := c.GetDay(plants[0].Id, year, month, day)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("plant_id,datetime,power_watt")
		for _, item := range records {
			t := time.Unix(int64(item.DateTime), 0).UTC()
			fmt.Printf("%d,%s,%.0f\n", item.PlantId, t.Format("2006-01-02T15:04:05Z"), item.GenerationPower)
		}
	}

}
