# solarmania

CLI to export production data from Solarman (solarmanpv.com).

**Warning** before you waste any time: I wrote this tool for my very limited use case. I only wanted to export production data for the only plant I have configured in Solarman. The Solarman API offers a lot more attributes (e. g. power consumption, feedin) which are not covered by the export, since I don't have any use for them.

Be aware that I cannot guarantee that this tool works for you, or will work for you in the future.

## Usage

0. Have Go installed
1. Clone the repository
2. Run `go run main.go --help`

## TODO

- Allow selection of the plant to export, or export for all plants. Currently only exports the first one found.
- Allow to specify the password via an environment variable or config file (security).
